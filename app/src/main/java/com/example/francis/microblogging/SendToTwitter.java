package com.example.francis.microblogging;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import winterwell.jtwitter.Twitter;

/**
 * Created by francis on 09/01/17.
 */

public class SendToTwitter extends AsyncTask<String, Integer, Integer>{
    private Context context;

    public SendToTwitter(Context context){
        this.context = context;
    }

    protected Integer doInBackground(String... params) {
        Twitter twitter;

        if(params.length != 3){
            Log.e("SendToTwitter", "Need 3 parameters but  " + params.length + " were provided");

            return 1;
        }

        twitter = new Twitter(params[0], params[1]);

        twitter.setAPIRootUrl("http://yamba.marakana.com/api");
        twitter.setStatus(params[2]);

        return 0;
    }

    protected void onPostExecute(Integer result) {
        CharSequence text;

        if(result != 0)
            text = "Send failed";
        else
            text = "Send succeeded";

        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}


