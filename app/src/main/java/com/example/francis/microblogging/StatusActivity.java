package com.example.francis.microblogging;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StatusActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String status;
                EditText textEdit;
                SendToTwitter task;

                textEdit = (EditText) findViewById(R.id.editText);

                status = textEdit.getText().toString();
                Log.d("StatusActivity", "Status : \"" + status + "\"");

                task = new SendToTwitter(v.getContext());

                task.execute("Eiffel", "pomme1", status);
            }
        });

    }
}
